function validateForm() {
    const login = document.forms["logging"]["login"].value;
    const password = document.forms["logging"]["password"].value;
    const repeatedPassword = document.forms["logging"]["repeated_password"].value;

    if (countWords(login) > 1) {
        alert("Login can't contain two or more words");
        return false;
    }
    if (password !== repeatedPassword) {
        alert("Repeated password doesn't match password");
        return false;
    }
    if (login.length < 4) {
        alert("Login should contain at least 4 symbols");
        return false;
    }
    if (password.length < 4 ){
        alert("Password should contain at least 4 symbols");
        return false;
    }
}

function countWords(str) {
    return str.split(' ').length;
}

function redirect() {
    setTimeout(function() {document.location.href="index.html" }, 3000);
}

function deltaLinear(progress) {
    return progress;
}

function deltaAccel(progress) {
    return Math.pow(progress, 2);
}

function deltaAccelDown(progress) {
    return (1 - deltaAccel(1 - progress));
}


function createAnimation(objid) {
    let picobj = document.getElementById(objid);
    let y = -150;
    let y2 = 0;
    let duration = 1000;
    let fps = 25;
    let fps_del = 1000 / 25;

    let start = new Date().getTime(); // Время старта



    let tm = setInterval(function () {
        let now = (new Date().getTime()) - start; // Текущее время

        let progress = now / duration; // Прогресс анимации - K - 0:1
        if (progress > 1)
            progress = 1;


        let newk = deltaAccelDown(progress);
        let newy = y - (y - y2) * newk;

        picobj.style.top = Math.round(newy) + "px";
        if (progress === 1)
            clearInterval(tm);

    }, fps_del);
}

function runAnim1(objid) {
    createAnimation(objid, "linear");
}
